"use strict";


!(function() {

    const imageType = ['natural', 'enhanced', 'aerosol', 'cloud'];

    const EpicApplication = {
        imageList: [],
        type: {},
        dateCache: {},
        imageCache: {}
    };

    imageType.forEach(type => {
        EpicApplication.dateCache[type] = null;
        EpicApplication.imageCache[type] = new Map();
    })

    document.addEventListener("DOMContentLoaded", function(){

        generateDomElements(imageType);
      
        const dateList = document.getElementById('image-menu');
        const img = document.getElementById('earth-image');
        const template = document.getElementById('image-menu-item');
        const imgTitle = document.getElementById('earth-image-title');
        const imgDate = document.getElementById('earth-image-date');

        const type = document.getElementById('type');
        type.addEventListener('change',  function() {
            getMostRecentDate(type.value);
        });
        const date = document.getElementById('date');
        
    
        
    
        document.getElementById('request_form')
            .addEventListener("submit", function(e) {
                e.preventDefault();
               if(!EpicApplication.imageCache[type.value].has(date.value)){ 
                console.log('fetching');
                fetch(`https://epic.gsfc.nasa.gov/api/${type.value}/date/${date.value}`)
                    .then(response => {
                        if(!response.ok) {
                            throw new Error("Network response was not ok");
                        }
                        return response.json();
                    })
                    .then(data => {
                        if (data.length === 0) {
                            displayErrorMessage();
                        } 
                        else {
                            EpicApplication.imageList = data;
                            EpicApplication.dateCache[type.value] = date.value;
                            EpicApplication.imageCache[type.value].set(date.value, data);
                            displayImagesInMenu(EpicApplication.imageList);
                           
                        }
                    })
                    .catch ( error => {
                        console.log("Error fetching data:", error);
                    });
                    console.log(EpicApplication)
                }
                else {
                    console.log(EpicApplication);
                    console.log("In else");
                
                    const cachedImages = EpicApplication.imageCache[type.value].get(date.value);
                
                    if (cachedImages) {
                        displayImagesInMenu(cachedImages);
                        
                    } else {
                        console.log('No cached images found for the specified date');
                    }
                }
    
            });

        

        function displayImagesInMenu(images){
            dateList.textContent = undefined;
            images.forEach((image, index) => {
                const menuItem = document.importNode(template.content, true);
                    
                menuItem.querySelector('li').setAttribute('data-image-list-index', index);
                menuItem.querySelector('.image-list-title').textContent = image.date;
                menuItem.querySelector('li').addEventListener('click', function() {
                    displaySelectedImage(index);
                    
                    imgDate.textContent = image.date;
                    imgTitle.textContent = image.caption;
                });
                    
                dateList.appendChild(menuItem);
            });
        };
    
        function displaySelectedImage(index) {
            let cutDate = date.value.split('-');
            const selectedImageURL = EpicApplication.imageCache[type.value].get(date.value)[index].image;
            img.src = `https://epic.gsfc.nasa.gov/archive/${type.value}/${cutDate[0]}/${cutDate[1]}/${cutDate[2]}/jpg/${selectedImageURL}.jpg`;
        }
        
            

        function getMostRecentDate(typeValue) {
            
                let dates = [];
                fetch(`https://epic.gsfc.nasa.gov/api/${typeValue}/all`)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error("Network response was not ok");
                        }
                        return response.json();
                    })
                    .then(data => {
                        dates = data;
                        dates.sort((a, b) => b - a);
                        const mostRecentDate = dates[0].date;
                        date.setAttribute('max', mostRecentDate);                            
                        EpicApplication.dateCache[typeValue] = mostRecentDate;
                    })
                    .catch(error => {
                        console.log("Error fetching data:", error);
                    });
        
        }
            

        function displayErrorMessage(){
            img.src = undefined;
            dateList.textContent = undefined;
            const errorMessage =  document.importNode(template.content, true);
            const span = errorMessage.querySelector('span');
            span.parentNode.removeChild(span);
            errorMessage.children[0].textContent = 'No available images for this date';
            dateList.appendChild(errorMessage);

            imgDate.textContent = undefined;
            imgTitle.textContent = undefined;
        }

        function generateDomElements(imageTypes){
            const typeDropDown = document.getElementById('type');
            typeDropDown.textContent = undefined;

            imageTypes.forEach(type => {
                const option = document.createElement('option');
                option.value = type;
                option.textContent = type;
                typeDropDown.appendChild(option);
            });
        }
    });
}());

